﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorBase : MonoBehaviour
{
    public int Hp { get; protected set; }
    public float Speed { get; protected set; }

    protected void Init(int hp, float speed)
    {
        Hp = hp;
        Speed = speed;
    }
}