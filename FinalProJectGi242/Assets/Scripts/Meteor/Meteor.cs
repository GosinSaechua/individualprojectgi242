﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

public class Meteor : MeteorBase, IDamagable
{
    public event Action OnExploded;
    
    public void Init(int hp, float speed)
    {
        base.Init(hp, speed);
    }

    public void TakeHit(int damage)
    {
        Hp -= damage;
        if(Hp > 0)
        {
            return;
        }

        Explode();
    }

    public void Explode()
    {
        Debug.Assert(Hp <= 0, "Hp is more than zero");
        gameObject.SetActive(false);
        Destroy(gameObject);
        OnExploded?.Invoke();
    }
    
}
