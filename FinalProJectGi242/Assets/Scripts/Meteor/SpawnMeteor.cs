﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMeteor : MonoBehaviour
{
    public GameObject asteroidPrefab;
    public float respawnTime = 1.0f;
    private Vector2 screenBounds;

    // Use this for initialization
    void Start () 
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        StartCoroutine(asteroidWave());
    }
    
    private void spawnasteroid()
    {
        GameObject a = Instantiate(asteroidPrefab);
        a.transform.position = new Vector2(Random.Range(-screenBounds.x , screenBounds.x),(screenBounds.y -2));
    }
    IEnumerator asteroidWave()
    {
        while(true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnasteroid();
        }
    }
}