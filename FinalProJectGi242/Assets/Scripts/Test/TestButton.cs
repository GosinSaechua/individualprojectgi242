﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Test
{
    public class TestButton : MonoBehaviour
    {
        public Button back;
        public Button selectLV;

        public void Start()
        {
            back.onClick.AddListener(Back);
            selectLV.onClick.AddListener(SelectLV);
        }
        
        public void Back()
        {
            SceneManager.LoadScene("Game");
        }
        
        public void SelectLV()
        {
            SceneManager.LoadScene("SelectLVscene");
        }
    }
}
