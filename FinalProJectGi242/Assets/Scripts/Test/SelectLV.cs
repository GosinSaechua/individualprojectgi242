﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SelectLV : MonoBehaviour
{
    public Button lv1;
    public Button lv2;

    public void Start()
    {
        lv1.onClick.AddListener(Onlv1buttonclicked);
        lv2.onClick.AddListener(Onlv2buttonclicked);
    }
        
    public void Onlv1buttonclicked()
    {
        SceneManager.LoadScene("Game");
    }
        
    public void Onlv2buttonclicked()
    {
        SceneManager.LoadScene("Gamelvhard");
    }
}
